create or replace PACKAGE BODY xx_test_soagwayZZZ427 AS

 FUNCTION get_ccid (P_SEGMENT1 IN VARCHAR2,
                    P_SEGMENT2 IN VARCHAR2,
                    P_SEGMENT3 IN VARCHAR2,
                    P_SEGMENT4 IN VARCHAR2,
                    P_SEGMENT5 IN VARCHAR2) RETURN NUMBER AS

 v_ccid    NUMBER;

 BEGIN

   SELECT code_combination_id INTO v_ccid
   FROM gl.gl_code_combinations
   WHERE NVL(segment1, ' ') = NVL(P_SEGMENT1, ' ')
   AND   NVL(segment2, ' ') = NVL(P_SEGMENT2, ' ')
   AND   NVL(segment3, ' ') = NVL(P_SEGMENT3, ' ')
   AND   NVL(segment4, ' ') = NVL(P_SEGMENT4, ' ')
   AND   NVL(segment5, ' ') = REPLACE(P_SEGMENT5, '´')
   AND   enabled_flag = 'Y';

   RETURN v_ccid;

  EXCEPTION
    WHEN no_data_found THEN
      RETURN -99;
    WHEN too_many_rows THEN
      RETURN -90;
  END get_ccid;
END xx_test_soagwayZZZ427;
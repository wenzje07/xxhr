CREATE OR REPLACE PACKAGE    HR_PERSON AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 

  FUNCTION GET_PERSON_DETAILS(P_PERSION_ID NUMBER) RETURN VARCHAR2;

END HR_PERSON;
/


CREATE OR REPLACE PACKAGE BODY    HR_PERSON AS

FUNCTION GET_PERSON_DETAILS(P_PERSION_ID NUMBER) RETURN VARCHAR2
IS
  l_error_msg  varchar2(2000);
BEGIN
   l_error_msg:=substr('&1 Error - &2. Please correct and resubmit transaction for Sabrix tax calculation. ',1,5);
  RETURN 'TEST';
END;

END HR_PERSON;
/

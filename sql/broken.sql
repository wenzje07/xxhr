CREATE or replace PACKAGE APPS.PACKAGE_CREATE AS

PROCEDURE testproc(in_id in number);

END PACKAGE_CREATE;
/
CREATE OR REPLACE PACKAGE BODY APPS.PACKAGE_CREATE AS
 PROCEDURE testproc(in_id in number) IS out_name varchar2(30);
   BEGIN
    SELECT sample_name INTO out_name
    FROM APPS.badTableName
    WHERE sample_id = in_id;
   END testproc;
 END PACKAGE_CREATE;
/
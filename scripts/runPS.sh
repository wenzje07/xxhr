#!/bin/ksh
# $Header: runPS.sh 120.0.12010000.8 2011/04/20 16:03:25 saskrish ship $

export PS_ROOT_DIR=$MSC_TOP/bin/PS/scp/12.1.3
export LD_LIBRARY_PATH=$PS_ROOT_DIR/common/bin:$PS_ROOT_DIR/ps/bin:$LD_LIBRARY_PATH
export CONNECTOR_HOME=$PS_ROOT_DIR/ps/config

showUsage()
{
    echo "Usage: $1 <dataStorePath> <planId> [-gzip]"
    exit 1
}

if [ $# -lt 2 ]
then
    showUsage $0
fi

#run pre-script
PRE_SCRIPT=$1/$2/script/pre_con.sh
if test -s $PRE_SCRIPT
then
    $PRE_SCRIPT $1 $2
    stat=$?
    if [ $stat != 0 ]; then
        exit $stat
    fi
fi

$PS_ROOT_DIR/common/start/run_ps_connector.sh $MSC_TOP/bin/PS/psConnectorRefresh.tcl $*

#run post-script
POST_SCRIPT=$1/$2/script/post_con.sh
if test -s $POST_SCRIPT
then
    $POST_SCRIPT $1 $2
    stat=$?
    if [ $stat != 0 ]; then
        exit $stat
    fi
fi

exit 0

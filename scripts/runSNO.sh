#!/bin/ksh
# $Header: runSNO.sh 120.2.12010000.6 2010/04/12 11:05:18 saskrish ship $

showUsage()
{
    echo "Usage: $1 <dataStorePath> <planId> [-solve] [-gzip]"
    exit 1
}

generateStepFile()
{
        cat > $SNO_RUN_STEPS_FILE <<@
import $SNO_MODEL_PATH
@

    if test -s $CUSTOM_BATCH_FILE
    then
        echo "$CUSTOM_BATCH_FILE exists."
        cat $CUSTOM_BATCH_FILE >> $SNO_RUN_STEPS_FILE
    fi


    if [ $1 -eq 1 ]
    then
        cat >> $SNO_RUN_STEPS_FILE <<@
solve
@
    fi

        cat >> $SNO_RUN_STEPS_FILE <<@
layout
symsave $2 $SNO_SOLVED_MODEL_PATH
@
}

if [ $# -lt 2 ]
then
    showUsage $0
fi

export LINXLOGDIR=$1/$2/refresh/target
#clear connector log 
cat > $LINXLOGDIR/integration.log <<@
@

#run pre-script
PRE_SCRIPT=$1/$2/script/pre_con.sh
if test -s $PRE_SCRIPT
then
    $PRE_SCRIPT > $LINXLOGDIR/integration.log 2>&1
    stat=$?
    if [ $stat != 0 ]; then
        exit $stat
    fi
fi


#run SNO connector
$MSC_TOP/bin/SNO/scp/12.1.3/common/start/run_sno_connector.sh $MSC_TOP/bin/SNO/snoConnectorRefresh.tcl $1 $2
stat=$?

#copy connector log to linx.log
cat $LINXLOGDIR/integration.log > $LINXLOGDIR/linx.log

#Check connector script status
if [ $stat != 0 ]; then
    exit $stat
fi

#run post-script
POST_SCRIPT=$1/$2/script/post_con.sh
if test -s $POST_SCRIPT
then
    $POST_SCRIPT >> $LINXLOGDIR/linx.log 2>&1
    stat=$?
    if [ $stat != 0 ]; then
        exit $stat
    fi
fi

CUSTOM_BATCH_FILE=$1/$2/script/SNOBatch

SNO_MODEL_PATH=$1/$2/refresh/target/model.imp
SNO_SOLVED_MODEL_PATH=$1/$2/refresh/target/EbiSNOModel.sym


# Create SNO run steps
SNO_RUN_STEPS_FILE=$1/$2/refresh/target/runSNOSteps.txt

if [[ -n $3 ]]
then
    if [ $3 = "-solve" ]
    then
        generateStepFile 1 $4
    else
        generateStepFile 0 $3
    fi
else
    generateStepFile 0
fi


$MSC_TOP/bin/SNO/scp/12.1.3/common/start/run_sno_snocon.sh < $SNO_RUN_STEPS_FILE

exit 0

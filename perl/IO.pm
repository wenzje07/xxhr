
# +===========================================================================+
# |   Copyright (c) 2003 Oracle Corporation, Redwood Shores, California, USA
# |                         All Rights Reserved
# |                        Applications Division
# +===========================================================================+
# |
# | FILENAME
# |   IO.pm
# |
# | DESCRIPTION
# |      TXK IO package
# |
# | USAGE
# |       See IO.html
# |
# | PLATFORM
# |
# | NOTES
# |
# +===========================================================================+

# $Header: IO.pm 120.0 2005/05/07 05:22:27 appldev noship $

package TXK::IO;

@ISA = qw( TXK::Common );

######################################
# Standard Modules
######################################

use strict;
use English;
use Carp;

require 5.005;

######################################
# Package Specific Modules
######################################

use Cwd;
use Symbol;

use TXK::Util();
use TXK::OSD();
use TXK::Error();

######################################
# Public Constants
######################################

use constant READ    => 0;
use constant WRITE   => 1;
use constant RDWR    => 2;
use constant APPEND  => 3;

use constant REF_FILE_STR => "<<REF_FILE_STR>>";

######################################
# Package Variables 
######################################

my $PACKAGE_ID = "TXK::IO";

my $TXK_LOG_DIR = "TXK_LOG_DIR";

######################################
# Object Keys
######################################

my $OPEN_MODE      = "mode";
my $BINARY_MODE    = "binaryMode";
my $HANDLE_NAME    = "handleName";
my $HANDLE_REF     = "handleRef";
my $IO_REF         = "IORef";
my $FILE_NAME      = "fileName";
my $DIR_NAME       = "dirName";
my $SAVE_HANDLE    = "saveHandle";
my $SAVE_REF       = "saveRef",    
my $IS_OPEN	   = "isOpen";
my $FILE_ID	   = "fileId";
my $AUTO_FLUSH	   = "autoFlush";
my $BUFFER         = "IOBUFFER";
my $BUFFER_SIZE    = "bufferSize";

######################################
#
# Object Structure
# ----------------
#
#  Hash Array
#
######################################

######################################
# Package Methods 
#
# Public
#
#	new 	- build empty object
#
######################################

sub new;
sub DESTROY;
sub open;
sub close;
sub read;
sub write;
sub readLine;
sub print;
sub getFileHandle;
sub chmod;
sub getmod;

######################################
# Package Methods
# 
# Private
#       All private methods are marked with a leading underscore.
#
######################################

sub _getModeStr;

######################################
# Constructor
######################################

sub new {
  my $type = $ARG[0];

  my $self = TXK::Common->new();

  bless $self, $PACKAGE_ID ;

  my $key;

  my %INIT_OBJ = (
		   PACKAGE_IDENT   => $PACKAGE_ID,
 		   $OPEN_MODE      => TXK::IO::READ,
 		   $HANDLE_NAME    => undef,
 		   $HANDLE_REF     => undef,
 		   $FILE_NAME      => undef,
 		   $DIR_NAME       => undef,
 		   $SAVE_HANDLE	=> undef,
 		   $SAVE_REF       => undef,
		   $IS_OPEN  	   => "0",
		   $FILE_ID	   => undef,
                   $AUTO_FLUSH     => undef,
	           $BINARY_MODE    => undef,
                  );

  foreach $key (keys %INIT_OBJ)
   {
     $self->{$key} = $INIT_OBJ{$key};
   }

  return $self;
}

######################################
# Destructor
######################################

sub DESTROY
{
}

######################################
# Open
######################################

sub open
{   
  my $self  = $ARG[0];
  my $args  = $ARG[1];

  TXK::Util->isValidObj({obj=>$self,package=>$PACKAGE_ID,args=>$args});

  TXK::Util->isValidArgs({args=>$args,reqd=>[]});

  return $self->setError("File already opened") if ( $self->{$IS_OPEN} );

  my $key ;
  my $actualValue;
  my $file_id;
  my $set_ref;

  $self->{$FILE_NAME} = "";
  $self->{$HANDLE_REF} = undef;

  foreach $key (keys %$args)
   {
#	Validate Args

     if ( exists $self->{$key} )
      {
        $actualValue = $args->{$key} ;

        if ( $key eq $OPEN_MODE )
         {
           return $self->setError("OPEN mode must be between 0 and 3")
                        if ( $args->{$key} < 0 || $args->{$key} > 3 );
         }
        elsif ( $key eq $SAVE_HANDLE )
         {
           return $self->setError("saveHandle must be either true or false")
                        unless ( $args->{$key} eq "true" ||
                                 $args->{$key} eq "false" ||
                                 $args->{$key} eq "0" );
           $actualValue = ( $args->{$key} eq "true" ? "true" : "0" );
         }
        elsif ( $key eq $AUTO_FLUSH )
         {
           return $self->setError("$AUTO_FLUSH must be either true or false")
                        unless ( $args->{$key} eq "true" ||
                                 $args->{$key} eq "false" ||
                                 $args->{$key} eq "0" );
           $actualValue = ( $args->{$key} eq "true" ? "true" : "0" );
         }
        elsif ( $key eq $BINARY_MODE )
         {
           return $self->setError("$BINARY_MODE must be either true or false")
                        unless ( $args->{$key} eq "true" ||
                                 $args->{$key} eq "false" ||
                                 $args->{$key} eq "0" );
           $actualValue = ( $args->{$key} eq "true" ? "true" : "0" );
         }
        elsif ( $key eq $HANDLE_REF )
         {
            return $self->setError("$HANDLE_REF must be a GLOB reference")
                   unless ( ref($args->{$key}) eq "GLOB" );
         }
        elsif ( $key eq $HANDLE_NAME )
         {
         }
        elsif ( $key eq $FILE_NAME )
         {
         }
        elsif ( $key eq $DIR_NAME )
         {
         }
        else
         {
           return $self->setError("Invalid argument : " . $key );
         }

        $self->{$key} = $actualValue;
      }
   }

   return $self->setError("$FILE_NAME must be specified")
          unless ( $self->{$FILE_NAME} || $self->{$HANDLE_REF} );
 
 if ( $self->{$HANDLE_NAME} )
  {
    if ( $self->{$SAVE_HANDLE} )
     {
       $file_id = _getModeStr($self) . "&" . $self->{$HANDLE_NAME};

       $self->{$SAVE_REF} = gensym;

       CORE::open($self->{$SAVE_REF}, $file_id)
            or return $self->setError({ error => "Cannot open SAVE HANDLE",
                                        file  => $file_id,
                                        errorno=>$ERRNO } );
     }

    $set_ref  = '$self->{$IO_REF} = \*' . $self->{$HANDLE_NAME} ;
 
    eval $set_ref;
  }
 else
  {
    $self->{$IO_REF} = gensym;
  }

  my $file_str;

  if ( $self->{$FILE_NAME} )
   {
     $file_str = TXK::OSD->trDirPath($self->{$DIR_NAME}) .
                 ($self->{$DIR_NAME} ? TXK::OSD->getDirSeparator() : undef) .
                 TXK::OSD->trFileName($self->{$FILE_NAME});

     $file_id = _getModeStr($self) . $file_str;

     CORE::open($self->{$IO_REF},$file_id)
         or return $self->setError({ error => "Unable to open file",
                                     file  => $file_id,
                                     errorno=>$ERRNO } );
   }
  else
   {
     local *OPEN_REF = *{$self->{$HANDLE_REF}} ;

     $file_str = REF_FILE_STR;

     CORE::open($self->{$IO_REF}, _getModeStr($self) . "&OPEN_REF")
         or return $self->setError({ error => "Unable to open file by ref",
                                     errorno=>$ERRNO } );
   }

 if ( $self->{$OPEN_MODE} == TXK::IO::WRITE &&
      $self->{$AUTO_FLUSH} )
  {
    my $oldfh = select($self->{$IO_REF});

    $OUTPUT_AUTOFLUSH = 1;

    select($oldfh);
  }

 binmode $self->{$IO_REF} if ( $self->{$BINARY_MODE} );

 $self->{$FILE_ID} = $file_str;
 $self->{$IS_OPEN} = "true";

 return TXK::Error::SUCCESS;
}                  

######################################
# close
######################################

sub close
{   
  my $self  = $ARG[0];
  my $args  = $ARG[1];

  TXK::Util->isValidObj({obj=>$self,package=>$PACKAGE_ID,args=>$args});

  return $self->setError("File not open") if ( ! $self->{$IS_OPEN} );

  CORE::close($self->{$IO_REF})
     or return $self->setError({ error => "Unable to close file",
                                 file  => $self->{$FILE_ID},
                                 errorno=>$ERRNO } );

  if ( $self->{$HANDLE_NAME} && $self->{$SAVE_HANDLE} )
   {
     local *RESET = $self->{$SAVE_REF} ;

     CORE::open($self->{$IO_REF}, _getModeStr($self) . "&RESET")
         or return $self->setError({ error => "Unable to reset save file",
                                     handle=> $self->{$HANDLE_NAME},
                                     errorno=>$ERRNO } );
   }

 $self->{$IS_OPEN} = "0";

 return TXK::Error::SUCCESS;
}

######################################
# read 
######################################

sub read 
{
 my $self  = $ARG[0];
 my $args  = $ARG[1];

 TXK::Util->isValidObj({obj=>$self,package=>$PACKAGE_ID,args=>$args});

 TXK::Util->isValidArgs({args=>$args,reqd=>["$BUFFER","$BUFFER_SIZE"]});

 return $self->setError("File not open") if ( ! $self->{$IS_OPEN} );

 return $self->setError("File opened for write, read not allowed")
                   if ( $self->{$OPEN_MODE} == TXK::IO::WRITE );

 return $self->setError("Input buffer must be a scalar ref")
                   if ( ref($args->{$BUFFER}) ne "SCALAR" );

 return $self->setError("Buffer size cannot be <= 0")
                   if ( $args->{$BUFFER_SIZE} <= 0 );

 my $rc = CORE::read($self->{$IO_REF},${$args->{$BUFFER}},
		     $args->{$BUFFER_SIZE});

 $self->setError({ error => "I/O error during read",
                          file  => $self->{$FILE_ID},
                          errorno=>$ERRNO,
                          _returnCode => -1 } )
            unless ( defined $rc );

 return $rc;
}

######################################
# readLine
######################################

sub readLine
{
 my $self  = $ARG[0];
 my $args  = $ARG[1];

 my $fh;

 TXK::Util->isValidObj({obj=>$self,package=>$PACKAGE_ID});

 return $self->setError("File not open") if ( ! $self->{$IS_OPEN} );

 return $self->setError("File opened for write, read not allowed")
                   if ( $self->{$OPEN_MODE} == TXK::IO::WRITE );

 $fh = $self->{$IO_REF};

 return <$fh>;
}

######################################
# write
######################################

sub write
{
  my $self  = $ARG[0];
  my $args  = $ARG[1];

  TXK::Util->isValidObj({obj=>$self,package=>$PACKAGE_ID,args=>$args});

  return $self->setError("File not open") if ( ! $self->{$IS_OPEN} );

  return $self->setError("File opened for read, write not allowed")
                   if ( $self->{$OPEN_MODE} == TXK::IO::READ )
}

######################################
# print
######################################

sub print
{
  my $self  = $ARG[0];
  my $args  = $ARG[1];

  my $fh;

  TXK::Util->isValidObj({obj=>$self,package=>$PACKAGE_ID});

  return $self->setError("File not open") if ( ! $self->{$IS_OPEN} );

  return $self->setError("File opened for read, write not allowed")
                   if ( $self->{$OPEN_MODE} == TXK::IO::READ );

  $fh = $self->{$IO_REF};

  print $fh $args or
              return $self->setError({ error => "Unable to print to file",
                                       handle=> $self->{$HANDLE_NAME},
				       fileid=> $self->{$FILE_ID},
                                       errorno=>$ERRNO } );

  return TXK::Error::SUCCESS;
}

######################################
# chmod
######################################

sub chmod
{
  my $self  = $ARG[0];
  my $args  = $ARG[1];

  TXK::Util->isValidObj({obj=>$self,package=>$PACKAGE_ID});

  return $self->setError("File not open") if ( ! $self->{$IS_OPEN} );

  chmod($args,$self->{$FILE_ID}) 
        or return $self->setError({error => "Unable to chmod file - mode $args",
                                   handle=> $self->{$HANDLE_NAME},
                                   fileid=> $self->{$FILE_ID},
                                   errorno=>$ERRNO } );

  return TXK::Error::SUCCESS;
}

######################################
# getmod
######################################

sub getmod
{
  my $self  = $ARG[0];
  my $args  = $ARG[1];

  TXK::Util->isValidObj({obj=>$self,package=>$PACKAGE_ID});

  return $self->setError("File not open") if ( ! $self->{$IS_OPEN} );

  my @statInfo = stat($self->{$IO_REF});  

  unless ( defined @statInfo )
   {
     $self->setError({ error => "Unable to stat file",
                       handle=> $self->{$HANDLE_NAME},
                       fileid=> $self->{$FILE_ID},
                       errorno=>$ERRNO } );

     return TXK::Error::WARNING;
   }

  return ( $statInfo[2] & 0777 );
}

######################################
# getFileHandle
######################################

sub getFileHandle
{
  my $self  = $ARG[0];
  my $args  = $ARG[1];

  TXK::Util->isValidObj({obj=>$self,package=>$PACKAGE_ID,args=>$args});

  return $self->setError("File not opened") unless ( $self->{$IS_OPEN} );

  return $self->{$IO_REF};
}

######################################
# End of Public methods
######################################

# ==========================================================================

sub _getModeStr
{ 
  my $self  = $ARG[0];
  my $args  = $ARG[1];

  return "<"     if ( $self->{$OPEN_MODE} == TXK::IO::READ );
  return ">"     if ( $self->{$OPEN_MODE} == TXK::IO::WRITE );
  return ">>"    if ( $self->{$OPEN_MODE} == TXK::IO::APPEND );

  return "+<";
}

# ==========================================================================

1;



# +===========================================================================+
# |   Copyright (c) 2003 Oracle Corporation, Redwood Shores, California, USA
# |                         All Rights Reserved
# |                        Applications Division
# +===========================================================================+
# |
# | FILENAME
# |   Error.pm
# |
# | DESCRIPTION
# |      TXK Error package
# |
# | USAGE
# |       See Error.html
# |
# | PLATFORM
# |
# | NOTES
# |
# +===========================================================================+

# $Header: Error.pm 120.0 2005/05/07 05:22:25 appldev noship $

package TXK::Error;

@ISA = qw( TXK::Common );

######################################
# Standard Modules
######################################

use strict;
use English;
use Carp qw(cluck croak confess);

require 5.005;

use TXK::Util();

######################################
# Public Constants
######################################

use constant FAIL               => "0";
use constant WARNING            => "-1";
use constant SUCCESS            => "1";

use constant WITH_STACKTRACE    => 10;
use constant NO_STACKTRACE      => 11;

######################################
# Package Variables 
######################################

my $PACKAGE_ID = "TXK::Error";

######################################
# Object Keys
######################################

######################################
#
# Object Structure
# ----------------
#
#  Hash Array
#
######################################

######################################
# Package Methods 
#
# Public
#
#	new 	- build empty object
#
######################################

sub new;
sub DESTROY;
sub abort;
sub printMsg;

######################################
# Package Methods
# 
# Private
#       All private methods are marked with a leading underscore.
#
######################################

######################################
# Constructor
######################################

sub new {
  my $type = $ARG[0];

  my $self = TXK::Common->new();

  bless $self, $PACKAGE_ID ;

  my $key;

  my %INIT_OBJ = (
                   PACKAGE_IDENT   => $PACKAGE_ID,
                  );

  foreach $key (keys %INIT_OBJ)
   {
     $self->{$key} = $INIT_OBJ{$key};
   }

  return $self;
}

######################################
# Destructor
######################################

sub DESTROY
{
}

######################################
# Abort
######################################

sub abort
{
  my $self  = $ARG[0];
  my $msg   = $ARG[1];
  my $info  = $ARG[2];

  my $msgtab = undef;
  my $infostr= undef;

  if ( defined $msg )
   {
     if ( ref($msg) eq "HASH" )
      {
        $msgtab = $msg;
      }
     else
      {
        $infostr = $msg;
        $msgtab = $info if ( defined $info && ref($info) eq "HASH" );
      }
   }

  my $fmtarg;

  if ( defined $infostr )
   {
     $fmtarg = ( defined $msgtab ? $msgtab : $infostr );
   }
  else
   {
     $fmtarg = $msgtab;
   }

  my $fmterr = _formatError($self,$fmtarg,TXK::Error::WITH_STACKTRACE,
                            ( defined $infostr && defined $msgtab 
                                     ? $infostr : undef ) );

  confess($fmterr);
}

######################################
# stop
######################################

sub stop
{
  my $self  = $ARG[0];
  my $msg   = $ARG[1];
  my $info  = $ARG[2];

  my $msgtab = {};
  my $infostr= "";

  if ( defined $msg )
   {
     if ( ref($msg) eq "HASH" )
      {
        $msgtab = $msg;
      }
     else
      {
        $infostr = $msg;
        $msgtab = $info if ( defined $info && ref($info) eq "HASH" );
      }
   }

  my $diestr = undef;

  $diestr = $infostr if ( $infostr ne "" );

  $diestr .= ( defined $diestr ? "\n" : "" ) . $msgtab->{'message'}
                             if ( exists($msgtab->{'message'}) );

  my $fmterr = _formatError($self,$diestr,TXK::Error::NO_STACKTRACE);

  die($fmterr . "\n" ) if ( defined $diestr );
  die("\n");

}

######################################
# PrintMsg
######################################

sub printMsg
{
  my $self  = $ARG[0];
  my $args  = $ARG[1];

  cluck(_formatError($self,$args,TXK::Error::WITH_STACKTRACE));
}

######################################
# End of Public methods
######################################

# ==========================================================================

######################################
# _formatError
######################################

sub _formatError
{
  my $self  = $ARG[0];
  my $args  = $ARG[1];
  my $stacktrace = $ARG[2];
  my $abortInfo = $ARG[3];	# Only used by abort.

  my ($msg,$actualargs);
  my $key;
  my $caller_level = 1;

#	Can be called as static/instance method and non-oo. Handle
#	all three cases.

  $actualargs = ( ref($self) eq $PACKAGE_ID || $self eq $PACKAGE_ID 
       			? $args : $self );

#	Args can be hash or string.

  $msg = "*******FATAL ERROR*******";
  $msg .= "\nPROGRAM : " . TXK::Runtime->getExecCommand() 
                         . "(" .  $PROGRAM_NAME . ")";
  $msg .= "\nTIME    : " . TXK::Util->getTimestamp() ;

#	The caller level is normally the calling subroutine. But if
#	level is specified use it instead.

  if ( ref($actualargs) eq "HASH" )
   {
     $caller_level = $actualargs->{'_caller_level'} 
                        if ( exists($actualargs->{'_caller_level'}) );
     delete $actualargs->{'_caller_level'}; # Don't print the caller_level
   }

#	We need to bump caller_level by 1 as we do formatting in
#	_formatError rather than Abort. This only affects the caller
#	function not the Level we display.

  $msg .= "\nFUNCTION: " . (caller($caller_level+1))[3]  . 
                           " [ Level $caller_level ] " ;

  if ( ref($actualargs) eq "HASH" )
   {
     $msg .= "\nERRORMSG: " . $abortInfo if ( defined $abortInfo );
     $msg .= "\nMESSAGES:";
     foreach $key (sort keys %$actualargs)
      {
        $msg .= "\n" ;
        $msg .= $key . " = " unless ( substr($key,0,1) eq "_" );
        $msg .= $actualargs->{$key};
      }
   }
  else
   {
     $msg .= "\nERRORMSG: " . $actualargs;
   }

  $msg .= "\n\n" . "STACK TRACE\n" 
            if ( defined $stacktrace &&
                $stacktrace == TXK::Error::WITH_STACKTRACE );

  return $msg;
}

######################################
# End of Public methods
######################################

1;

